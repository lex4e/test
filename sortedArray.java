import java.util.Random;

class SortedArray 
{
	public static void main(String[] args) 
	{
		Random random = new Random();
		int countEl = random.nextInt(10);
		Integer[] sortedArray = new Integer[countEl];
		
		Integer[] myArray = new Integer[countEl];
		for (int i=0; i<countEl; i++) {
			int randomNumber = random.nextInt(100);	
			myArray[i] = randomNumber;
		}
		printArray(myArray);	
		sortedArray = sortArray(myArray);
		printArray(sortedArray);	
	}
	
	public static Integer[] sortArray(Integer[] myArray) 
	{
		int countEl = myArray.length;
		Integer[] sortedArray = new Integer[countEl];
		Integer maxVal;
		int indexSortedArray = 0;
		while (myArray.length > 0) 
		{
			int indexTmpArray = 0;
			Integer[] tmpArray = new Integer[countEl-1];
			maxVal = myArray[0];
			for (int i=1; i < myArray.length; i++) 
			{
				if (myArray[i] > maxVal) 
				{
					tmpArray[indexTmpArray] = maxVal;
					maxVal = myArray[i];
				} else 
				{
					tmpArray[indexTmpArray] = myArray[i];
				}	
				indexTmpArray++;
			}
			myArray = tmpArray;
			sortedArray[indexSortedArray] = maxVal;
			indexSortedArray++;
			countEl--;
		}
		return sortedArray;
	}
	
	public static void printArray(Integer[] ar) 
	{
		String str = "[ ";
		for (Integer el : ar) 
		{
			str += el + " ";
		}
		str += "]";
		System.out.println(str);
	}
}